#!/usr/bin/env bash

cd $(dirname $0)

. init_homedir.sh

mkdir -p $HOME/mnt/sshfs $HOME/mnt/encfs/dropbox

if test "$DISPLAY"
then
    for i in `seq 5`
    do
        test -z "`mount |grep encfs|grep $HOME/secret`" && encfs --extpass=ssh-askpass $HOME/secret-crypted/ $HOME/secret -oallow_other
    done
else
    for i in `seq 5`
    do
        test -z "`mount |grep encfs|grep $HOME/secret`" && encfs $HOME/secret-crypted/ $HOME/secret -oallow_other
    done
fi

if test `cat $HOME/secret/.init/mnt/encfs/dropbox`
then
    test "$(mount|grep $HOME/mnt/encfs/dropbox|grep encfs)" || echo -e `cat $HOME/secret/.init/mnt/encfs/dropbox`|encfs -S $HOME/Dropbox/crypted $HOME/mnt/encfs/dropbox/
    #test "$(ps x|grep yandex-disk|grep -v grep)" || yandex-disk start &
else
    echo no secret
fi

test "$(ps x|grep afuse|grep -v grep)" || afuse -omount_template="sshfs %r:/ %m",unmount_template="fusermount -u %m",timeout=30,flushwrites $HOME/mnt/sshfs

