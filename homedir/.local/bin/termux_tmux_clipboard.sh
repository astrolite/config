#!/bin/bash

tmux setw -g xterm-keys on

tmux bind -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "termux-clipboard-set"
tmux bind-key ] run "tmux set-buffer \"\$(termux-clipboard-get)\"; tmux paste-buffer"

#tmux set-option -g mouse on
#tmux set-option -s set-clipboard off
#tmux bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "termux-clipboard-set"

