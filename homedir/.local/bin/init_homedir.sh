#!/usr/bin/env bash

cd $(dirname $0)

mkdir -p $HOME/.local/lib/sh
cat <<EOT > $HOME/.local/lib/sh/common.sh
alias m="mount "
alias me="mcedit "
alias ll="ls -la "
alias l="ls -1 "
alias p="mplayer -zoom -vo x11 -ao alsa"
alias fu="fusermount -u "
alias oo="libreoffice "
alias power="echo \\\$((\\\$(cat /sys/class/power_supply/BAT0/energy_now)00 / \\\$(cat /sys/class/power_supply/BAT0/energy_full)))"
alias ipython="ipython --no-confirm-exit"

export PATH=$(pwd):\$HOME/.local/bin:\$PATH

#export LANG=ru_RU.UTF-8
export EDITOR=nvim
alias editor=\$EDITOR

export HISTCONTROL=ignoreboth
shopt -s histappend
export PROMPT_COMMAND="\${PROMPT_COMMAND:+\$PROMPT_COMMAND\$'\n'}history -a; history -c; history -r"

if [[ -f \$HOME/.local/var/run/ssh_agent.sh ]]
then
    . \$HOME/.local/var/run/ssh_agent.sh
fi

EOT

read -r -d '' files <<-"EOFF__"
.gitconfig_user
.vimrc
.tmux.conf
.config/mc/ini
.config/mc/mc.ext
.config/mc/menu
.config/nvim/init.vim
.moc/config
.mplayer/config
EOFF__

FROM=$(pwd)/../..
TO=$HOME

for i in $files
do
    echo $i
    rm -f $TO/$i
    mkdir -p $(dirname $TO/$i)
    ln -rs $FROM/$i $TO/$i
done

sed -ri '/# addition to bashrc/d' $HOME/.bashrc
echo '. $HOME/.local/lib/sh/common.sh # addition to bashrc' >> $HOME/.bashrc

sed -ri '/# addition to gitconfig/d' $HOME/.gitconfig
echo '[include] path = .gitconfig_user # addition to gitconfig' >> $HOME/.gitconfig

