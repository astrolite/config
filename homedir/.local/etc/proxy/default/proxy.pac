// Файл Proxy Auto-Configuration (PAC) для автоматического переключения i2p, TOR и прямым соединением.
function FindProxyForURL(url, host)
{
if ((dnsDomainIs(host, ".i2p")) || (dnsDomainIs(host, ".onion"))) {

  if (dnsDomainIs(host, ".i2p")) {
   return "PROXY 127.0.0.1:4444"; //Перенаправить в i2p
        } else {
   return "SOCKS5 127.0.0.1:9050"; //Перенаправить в TOR
        }

    } else {
   //Список доменов для подключения через TOR
  var domains = //["*.*"];
  ["*lib.rus.ec", "*booktracker.org", "*flibusta.is", "*librusec.pro", "*avinfo.guru", "*2ip.ru", "*lostfilm.*", "*web.archive.org", "*forum.devsaid.com", "*linkedin.com", "*geti2p.net", "*i2p2.de", "*torrentdownloads.me", "*rutracker.nl", "*lurkmore.to"];

  var n = 0;

  for (var i = 0; i < domains.length; i++) {
   if (shExpMatch(host, domains[i])) {
    n = 1;
   }
  }

  if (n == 1) {
   return "SOCKS5 127.0.0.1:9050"; //Перенаправить в TOR
  } else {
   return "DIRECT";
  }
    }
}
