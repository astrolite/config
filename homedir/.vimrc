set number
set laststatus=2
set nocompatible
set timeoutlen=200
inoremap fd <ESC>
inoremap df <ESC>
inoremap jk <Enter>
inoremap kj <Enter>
inoremap kl <Backspace>
inoremap lk <Backspace>

"nnoremap y<Space> :call system('xsel -ib',getreg('"',1,1))<CR>
"nnoremap <Space>y :call system('xsel -ib',getreg('"',1,1))<CR>
nnoremap p<Space> :call setreg('"', system('xsel -ob'))<CR>p
nnoremap <Space>p :call setreg('"', system('xsel -ob'))<CR>p
nnoremap P<Space> :call setreg('"', system('xsel -ob'))<CR>P
nnoremap <Space>P :call setreg('"', system('xsel -ob'))<CR>P
"vnoremap y<Space> y:call system('xsel -ib',getreg('"',1,1))<CR>
"vnoremap <Space>y y:call system('xsel -ib',getreg('"',1,1))<CR>

""nnoremap yy yy:call system('xsel -ib',add(getreg('"',1,1), "\n"))<CR>
""nnoremap Y Y:call system('xsel -ib',add(getreg('"',1,1), "\n"))<CR>
"nnoremap yy yy:call system('xsel -ib',getreg('"',1,1))<CR>
"nnoremap Y Y:call system('xsel -ib',getreg('"',1,1))<CR>
"nnoremap y y:call system('xsel -ib',getreg('"',1,1))<CR>
"vnoremap y y:call system('xsel -ib',getreg('"',1,1))<CR>
"vnoremap Y Y:call system('xsel -ib',getreg('"',1,1))<CR>
""nnoremap p :call setreg('"', system('xsel -ob'))<CR>p
""nnoremap P :call setreg('"', system('xsel -ob'))<CR>P


set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set encoding=utf-8
set fileencoding=utf-8

"autocmd InsertEnter * set timeoutlen=200
"autocmd InsertLeave * set timeoutlen=1000

"t_Co=256
syntax on

filetype off

"call pathogen#helptags()
"call pathogen#infect()

filetype plugin indent on

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
"autocmd BufReadPost *
"  \ if line("'\"") > 0 && line("'\"") <= line("$") |
"  \   exe "normal g`\"" |
"  \ endif

"au VimEnter * RainbowParenthesesToggle
"
"

"set clipboard=unnamedplus
function! ClipboardOrXclip(command, register)
    echo "wwww" a:command a:register has('xterm_clipboard') has('gui_running')
    " https://vi.stackexchange.com/questions/332/define-custom-commands-for-the-and-registers
    if a:register !~ '[+*]' || has('xterm_clipboard') || has('gui_running')
        " Just return the original command if the clipboard is accessible
        " or it's not a register that should be handled by xsel
        return a:command
    endif
    if a:register == '+'
        return "<Esc>:r !xsel -bo<CR>"
    else
        return "<Esc>:r !xsel -po<CR>"
    endif
endfunction

"nnoremap <silent> <expr> p ClipboardOrXclip('p', v:register)
"set clipboard+=unnamed
"nnoremap <silent> p :echo v:register<cr>


"set clipboard=unnamed
""set clipboard=unnamedplus
"
autocmd TextYankPost * call system('xsel -i', @")
"
"" https://stackoverflow.com/questions/44480829/how-to-copy-to-clipboard-in-vim-of-bash-on-windows
"function! Paste(mode, reg)
"    call system('cat > /tmp/qa', a:reg)
"    echo a:reg v:register getregtype(v:register)
"    if a:reg =~ '[+*]'
""        let @" = system('xsel -o')
"    endif
"    return a:mode
"endfunction
"
""map <expr> p Paste('p', v:register)
""map <expr> P Paste('P', v:register)
"
"nnoremap <silent> <expr> p Paste('p', v:register)
"nnoremap <silent> <expr> P Paste('P', v:register)



"nnoremap <expr> y (v:register ==# '"' ? '"+' : '') . 'y'
"nnoremap <expr> yy (v:register ==# '"' ? '"+' : '') . 'yy'
"nnoremap <expr> Y (v:register ==# '"' ? '"+' : '') . 'Y'
"xnoremap <expr> y (v:register ==# '"' ? '"+' : '') . 'y'
"xnoremap <expr> Y (v:register ==# '"' ? '"+' : '') . 'Y'


