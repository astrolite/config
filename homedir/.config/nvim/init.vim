set number
set laststatus=2

" curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
" nvim
" :PlugInstall


"" Specify a directory for plugins
""call plug#begin('~/.vim/plugged')
"call plug#begin('~/.config/nvim/plugged')
"
"" One of following
"Plug 'ctrlpvim/ctrlp.vim'
"Plug 'junegunn/fzf'
"Plug 'liuchengxu/vim-clap'
"
"" Requires
"Plug 'guns/vim-sexp',    {'for': 'clojure'}
"Plug 'liquidz/vim-iced', {'for': 'clojure'}
"
"call plug#end()
"
"" Enable vim-iced's default key mapping
"" This is recommended for newbies
"let g:iced_enable_default_key_mappings = v:true



" Mimic Emacs Line Editing in Insert Mode Only
inoremap <C-A> <Home>
inoremap <C-B> <Left>
inoremap <C-E> <End>
inoremap <C-F> <Right>
" â is <Alt-B>
inoremap â <C-Left>
" æ is <Alt-F>
inoremap æ <C-Right>
inoremap <C-K> <Esc>lDa
inoremap <C-U> <Esc>d0xi
inoremap <C-Y> <Esc>Pa
inoremap <C-X><C-S> <Esc>:w<CR>a

inoremap <A-x> <Esc>:
inoremap <A-f> <Esc>lwi
inoremap <A-b> <Esc>bi
inoremap <A-S-f> <Esc>lWi
inoremap <A-S-b> <Esc>Bi

set tabstop=4     " (ts) width (in spaces) that a <tab> is displayed as
set expandtab     " (et) expand tabs to spaces (use :retab to redo entire file)
set shiftwidth=4  " (sw) width (in spaces) used in each step of autoindent (aswell as << and >>)

:colorscheme desert

