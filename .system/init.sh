#!/bin/bash


cd $(dirname $0)/..

git init .
git-crypt init
git-crypt export-key git-crypt.key
gpg -co git-crypt-key-1.gpg git-crypt.key
rm git-crypt.key
mv git-crypt-key-1.gpg .system/git-crypt-key-1.gpg
ln -rs .system/git-crypt-key-1.gpg .system/git-crypt-key.gpg
git-crypt add-gpg-user --no-commit --trusted AEA9AC49B6760CE32BAFA891BBD128760C52707D
git add .
git commit -mInit
git remote add origin git@gitlab.com:astrolite/config.git
git push --set-upstream origin --all
git push --set-upstream origin --tags


