#!/bin/bash

set -e
cd $(dirname $0)/..

git-crypt unlock <(gpg2 -qdo - .system/git-crypt-key.gpg)
