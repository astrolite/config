# Обход блокировки docker.io

## через конфиг докера (как зеркало docker.io)

расположен в

| операционная система | путь к файлу конфигурации |
|-------------------------------|--------------------------------------|
| Linux, regular setup | /etc/docker/daemon.json |
| Linux, rootless mode | ~/.config/docker/daemon.json |
| Windows | C:\ProgramData\docker\config\daemon.json |

конфиг:
```json
{ "registry-mirrors" : [ "https:\/\/huecker.io" ] }
```

```sh
cat << EOF | sudo tee -a /etc/docker/daemon.json
{ "registry-mirrors" : [ "https://dockerhub.timeweb.cloud", "https://huecker.io", "https://mirror.gcr.io", "https://c.163.com", "https://registry.docker-cn.com", "https://daocloud.io" ] }
EOF
```

теперь при попытке загрузки образа, докер будет сначала пытаться использовать прокси


## явное указание адреса

```sh
docker pull huecker.io/library/alpine:latest
docker pull cr.yandex/mirror/ubuntu:20.04
docker pull dh-mirror.gitverse.ru/apache/hadoop:3
```

| другие прокси | адрес описание |
|--------------------|----------------------|
| dh-mirror.gitverse.ru | Сбер |
| https://mirror.gcr.io | гугл |
| https://daocloud.io | Китай |
| https://c.163.com | Китай |
| https://registry.docker-cn.com | Китай |
| https://docker.mirrors.ustc.edu.cn | |
| eu-central-1.mirror.aliyuncs.com | |
| yandex.cr/mirror | |
| https://mirror.gcr.io | зеркало Google |
| https://dockerhub.timeweb.cloud | зеркало Timeweb |
| https://dockerhub1.beget.com | зеркало Бегет |
| https://c.163.com | зеркало Китай |
| https://registry.docker-cn.com | зеркало Китай |
| https://daocloud.io | зеркало Китай |
| https://cr.yandex/mirror | зеркало Яндекс |
| https://noohub.ru | зеркало noosoft |
| https://quay.io | зеркало Redhat |
| https://registry.access.redhat.com | зеркало Redhat |
| https://registry.redhat.io | зеркало Redhat |
| https://public.ecr.aws | зеркало Amazon |

## Авто-фикс файла daemon.json для Linux

Взял [тут](https://artydev.ru/posts/docker/)

Источник: [https://github.com/StasPlov/docker-unlock/tree/main](https://github.com/StasPlov/docker-unlock/tree/main)

```sh
wget -O unlock.sh https://raw.githubusercontent.com/StasPlov/docker-unlock/main/unlock.sh ; \
chmod +x unlock.sh ; \
./unlock.sh
```

