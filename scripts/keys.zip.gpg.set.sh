#!/bin/bash

set -e
cd $(dirname $0)

cp keys.zip.gpg /tmp/keys.zip.gpg
gpg /tmp/keys.zip.gpg
unzip -d /tmp/keys.zip.dir /tmp/keys.zip
cp -r /tmp/keys.zip.dir $HOME/tmp
cp -r /tmp/keys.zip.dir/.gnupg /tmp/keys.zip.dir/.ssh $HOME
rm -rf /tmp/keys.zip.gpg /tmp/keys.zip /tmp/keys.zip.dir

