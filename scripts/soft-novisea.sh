sudo apt install -y \
screen tmux mc neovim git \
language-pack-gnome-ru \
gnome-tweaks \
ecryptfs-utils cryptsetup rsync \
curl apt-transport-https \
gnome-shell-extension-gpaste gpaste \
eog gnupg2 git-crypt \
gedit gnome-text-editor

curl -fSsL https://repo.yandex.ru/yandex-browser/YANDEX-BROWSER-KEY.GPG | sudo gpg --dearmor | sudo tee /usr/share/keyrings/yandex.gpg
echo deb [arch=amd64 signed-by=/usr/share/keyrings/yandex.gpg] http://repo.yandex.ru/yandex-browser/deb stable main | sudo tee /etc/apt/sources.list.d/yandex-stable.list
sudo apt update
sudo apt install -y yandex-browser-stable
